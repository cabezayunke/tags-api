import TagModel from '../model/TagModel';
import ParamValidator from '../validator/ParamValidator';
import HttpStatus from 'http-status';
import Promise from 'bluebird';


/**
 * Tags API controller
 * (will be used by the router)
 */
export default class TagsController {

    /**
     * Get entities
     *
     * @param req
     * @param res
     * @param next
     */
    getTags(req, res, next) {
        // set default pagination to get the first 10 items sorted by ID DESC
        let options = {
            limit:  req.query.hasOwnProperty('limit') ? parseInt(req.query.limit, 10) : 10,
            offset: req.query.hasOwnProperty('offset') ? parseInt(req.query.offset, 0) : 0,
            sort: {
                _id: req.query.hasOwnProperty('order') ? req.query.order : 'desc'
            }
        };

        // paginate already uses bluebird promises
        TagModel.paginate({}, options)
            .then((result) => {
                res.json(result);
            })
            .catch((err) => {
                next(err);
            });
    };

    /**
     * Get entity by ID
     *
     * @param req
     * @param res
     * @param next
     */
    getTagById(req, res, next) {

        let id = req.params.id;
        let isValid = ParamValidator.validateMongoId(id);
        console.log(isValid);

        if(isValid) {
            console.log(id);

            // bluebird promise to return entity by ID
            TagModel.findById(id)
            .then((data) => {
                if(data) {
                    res.json(data);
                }
                else {
                    res.sendStatus(HttpStatus.NOT_FOUND);
                }
            })
            .catch((err) => {
                console.warn(err);
                next(err);
            });

        }
        else {
            // validation error
            // to be consistent, we use the same error format here as the payload-validator
            res.status(HttpStatus.BAD_REQUEST).json(ParamValidator.getValidationError('_id'));
        }
    };

    /**
     * Creates new entity
     *
     * @param req
     * @param res
     * @param next
     */
    crateNewTag(req, res, next) {

        let isValid = ParamValidator.validateBody(
            req.body, // data
            [ // mandatory params
                '_id', 'tags'
            ]
        );

        if(isValid.success) {
            let newEntity = new TagModel(req.body);
            // bluebird promise to return saved entity
            newEntity.save()
            .then(() => {
                console.log('New entity created');
                res.status(HttpStatus.CREATED).json(newEntity);
            })
            .catch((err) => {
                console.warn(err);
                next(err);
            });
        }
        else {
            res.status(HttpStatus.BAD_REQUEST).json(isValid.response);
        }

    };

    /**
     * Delete entity
     *
     * @param req
     * @param res
     * @param next
     */
    deleteTag(req, res, next) {

        let id = req.params.id;
        let isValid = ParamValidator.validateMongoId(id);
        console.log(isValid);

        if(isValid) {
            console.log(id);
            // bluebird promise to find entity first
            TagModel.findById(id)
            .then((data) => {
                if(data) {
                    console.log('Entity to delete found');
                    // bluebird promise to delete entity
                    return data.remove()
                }
                else {
                    res.sendStatus(HttpStatus.NOT_FOUND);
                }
            })
            .then(() => {
                console.log('Entity successfully deleted!');
                res.sendStatus(HttpStatus.OK);
            })
            .catch((err) => {
                console.warn(err);
                next(err);
            });

        }
        else {
            // validation error
            // to be consistent, we use the same error format here as the payload-validator
            res.status(HttpStatus.BAD_REQUEST).json(ParamValidator.getValidationError('_id'));
        }
    }


    /**
     * Updates an entity
     *
     * @param req
     * @param res
     * @param next
     */
    updateTag(req, res, next) {

        let id = req.params.id;
        let isValidId = ParamValidator.validateMongoId(id);

        let isValidBody = ParamValidator.validateBody(
            req.body, // data
            [] // mandatory params
        );

        if(isValidId) {
            if(isValidBody.success) {
                // bluebird promise to return updated entity
                // we need the 'new' param to return the updated entity
                TagModel.findByIdAndUpdate(id, { $set: req.body }, {new: true})
                .then((data) => {
                    if(data) {
                        console.log('Entity updated');
                        res.status(HttpStatus.OK).json(data);
                    }
                    else {
                        res.sendStatus(HttpStatus.NOT_FOUND);
                    }
                })
                .catch((err) => {
                    console.warn(err);
                    next(err);
                });
            }
            else {
                res.status(HttpStatus.BAD_REQUEST).json(isValidBody.response);
            }
        }
        else {
            // validation error
            // to be consistent, we use the same error format here as the payload-validator
            res.status(HttpStatus.BAD_REQUEST).json(ParamValidator.getValidationError('_id'));
        }
    };

    /*
     * Custom controller methods here
     */

    /*
     * END of custom controller methods
     */
}