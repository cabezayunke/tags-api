import { Router } from 'express';
import TagsController from './controller/TagsController';

const router = new Router();

/*
 * Tags API routes
 */
const controller = new TagsController();
router.get('/tags/:id', controller.getTagById);
router.get('/tags', controller.getTags);
router.post('/tags', controller.crateNewTag);
router.delete('/tags/:id', controller.deleteTag);
router.put('/tags/:id', controller.updateTag);

/*
 * Custom routes
 */

/*
 * END of custom routes
 */

export default router;