import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
import Promise from 'bluebird';

// make mongoose use bluebird
mongoose.Promise = Promise;

/**
 * Mongoose schema
 */
const schema = mongoose.Schema(
    {
        _id: {
            type: mongoose.Schema.Types.ObjectId,
            index: true,
            required: true
        },
        tags: {
            type: [String],
            required: true
        }
    }
);

schema.plugin(mongoosePaginate);

// create model from schema
const TagModel = mongoose.model('TagModel', schema);
export default TagModel;