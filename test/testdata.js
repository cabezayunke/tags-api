module.exports = {
    "entitySample": {
        "__v": 0,
        "_id": "5984a997f3bf94001cb97bec",
        "tags": ["new", "tutorial"]
    },
    "updatedEntitySample": {
        "__v": 0,
        "_id": "5984a997f3bf94001cb97bec",
        "tags": ["new", "tutorial", "updated_tag"]
    },
    "invalidIds": ["invalidId", 3245345, null],
    "invalidPostData": [
        {
            "_id": "5984a997f3bf94001cb97bec"
        },
        {
            "tags": ["new", "tutorial", "updated_tag"]
        },
        {
            "_id": "5984a997f3bf94001cb97bec",
            "tags": []
        },
        {
            "_id": "5984a997f3bf94001cb97bec",
            "tags": null
        },
        {
            "_id": "5984a997f3bf94001cb97bec",
            "tags": "dsfgsdfg"
        },
        {
            "_id": "5984a997f3bf94001cb97bec",
            "tags": 245673467
        }
    ],
    "invalidPutData": [
        {
            "_id": "5984a997f3bf94001cb97bec",
            "tags": []
        },
        {
            "_id": "5984a997f3bf94001cb97bec",
            "tags": null
        },
        {
            "_id": "5984a997f3bf94001cb97bec",
            "tags": "dsfgsdfg"
        },
        {
            "_id": "5984a997f3bf94001cb97bec",
            "tags": 245673467
        }
    ]
};