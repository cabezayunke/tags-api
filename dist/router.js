'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _TagsController = require('./controller/TagsController');

var _TagsController2 = _interopRequireDefault(_TagsController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = new _express.Router();

/*
 * Tags API routes
 */
var controller = new _TagsController2.default();
router.get('/tags/:id', controller.getTagById);
router.get('/tags', controller.getTags);
router.post('/tags', controller.crateNewTag);
router.delete('/tags/:id', controller.deleteTag);
router.put('/tags/:id', controller.updateTag);

/*
 * Custom routes
 */

/*
 * END of custom routes
 */

exports.default = router;