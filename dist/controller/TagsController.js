'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _TagModel = require('../model/TagModel');

var _TagModel2 = _interopRequireDefault(_TagModel);

var _ParamValidator = require('../validator/ParamValidator');

var _ParamValidator2 = _interopRequireDefault(_ParamValidator);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Tags API controller
 * (will be used by the router)
 */
var TagsController = function () {
    function TagsController() {
        _classCallCheck(this, TagsController);
    }

    _createClass(TagsController, [{
        key: 'getTags',


        /**
         * Get entities
         *
         * @param req
         * @param res
         * @param next
         */
        value: function getTags(req, res, next) {
            // set default pagination to get the first 10 items sorted by ID DESC
            var options = {
                limit: req.query.hasOwnProperty('limit') ? parseInt(req.query.limit, 10) : 10,
                offset: req.query.hasOwnProperty('offset') ? parseInt(req.query.offset, 0) : 0,
                sort: {
                    _id: req.query.hasOwnProperty('order') ? req.query.order : 'desc'
                }
            };

            // paginate already uses bluebird promises
            _TagModel2.default.paginate({}, options).then(function (result) {
                res.json(result);
            }).catch(function (err) {
                next(err);
            });
        }
    }, {
        key: 'getTagById',


        /**
         * Get entity by ID
         *
         * @param req
         * @param res
         * @param next
         */
        value: function getTagById(req, res, next) {

            var id = req.params.id;
            var isValid = _ParamValidator2.default.validateMongoId(id);
            console.log(isValid);

            if (isValid) {
                console.log(id);

                // bluebird promise to return entity by ID
                _TagModel2.default.findById(id).then(function (data) {
                    if (data) {
                        res.json(data);
                    } else {
                        res.sendStatus(_httpStatus2.default.NOT_FOUND);
                    }
                }).catch(function (err) {
                    console.warn(err);
                    next(err);
                });
            } else {
                // validation error
                // to be consistent, we use the same error format here as the payload-validator
                res.status(_httpStatus2.default.BAD_REQUEST).json(_ParamValidator2.default.getValidationError('_id'));
            }
        }
    }, {
        key: 'crateNewTag',


        /**
         * Creates new entity
         *
         * @param req
         * @param res
         * @param next
         */
        value: function crateNewTag(req, res, next) {

            var isValid = _ParamValidator2.default.validateBody(req.body, // data
            [// mandatory params
            '_id', 'tags']);

            if (isValid.success) {
                var newEntity = new _TagModel2.default(req.body);
                // bluebird promise to return saved entity
                newEntity.save().then(function () {
                    console.log('New entity created');
                    res.status(_httpStatus2.default.CREATED).json(newEntity);
                }).catch(function (err) {
                    console.warn(err);
                    next(err);
                });
            } else {
                res.status(_httpStatus2.default.BAD_REQUEST).json(isValid.response);
            }
        }
    }, {
        key: 'deleteTag',


        /**
         * Delete entity
         *
         * @param req
         * @param res
         * @param next
         */
        value: function deleteTag(req, res, next) {

            var id = req.params.id;
            var isValid = _ParamValidator2.default.validateMongoId(id);
            console.log(isValid);

            if (isValid) {
                console.log(id);
                // bluebird promise to find entity first
                _TagModel2.default.findById(id).then(function (data) {
                    if (data) {
                        console.log('Entity to delete found');
                        // bluebird promise to delete entity
                        return data.remove();
                    } else {
                        res.sendStatus(_httpStatus2.default.NOT_FOUND);
                    }
                }).then(function () {
                    console.log('Entity successfully deleted!');
                    res.sendStatus(_httpStatus2.default.OK);
                }).catch(function (err) {
                    console.warn(err);
                    next(err);
                });
            } else {
                // validation error
                // to be consistent, we use the same error format here as the payload-validator
                res.status(_httpStatus2.default.BAD_REQUEST).json(_ParamValidator2.default.getValidationError('_id'));
            }
        }

        /**
         * Updates an entity
         *
         * @param req
         * @param res
         * @param next
         */

    }, {
        key: 'updateTag',
        value: function updateTag(req, res, next) {

            var id = req.params.id;
            var isValidId = _ParamValidator2.default.validateMongoId(id);

            var isValidBody = _ParamValidator2.default.validateBody(req.body, // data
            [] // mandatory params
            );

            if (isValidId) {
                if (isValidBody.success) {
                    // bluebird promise to return updated entity
                    // we need the 'new' param to return the updated entity
                    _TagModel2.default.findByIdAndUpdate(id, { $set: req.body }, { new: true }).then(function (data) {
                        if (data) {
                            console.log('Entity updated');
                            res.status(_httpStatus2.default.OK).json(data);
                        } else {
                            res.sendStatus(_httpStatus2.default.NOT_FOUND);
                        }
                    }).catch(function (err) {
                        console.warn(err);
                        next(err);
                    });
                } else {
                    res.status(_httpStatus2.default.BAD_REQUEST).json(isValidBody.response);
                }
            } else {
                // validation error
                // to be consistent, we use the same error format here as the payload-validator
                res.status(_httpStatus2.default.BAD_REQUEST).json(_ParamValidator2.default.getValidationError('_id'));
            }
        }
    }]);

    return TagsController;
}();

exports.default = TagsController;