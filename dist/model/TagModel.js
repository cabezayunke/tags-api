'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _mongoosePaginate = require('mongoose-paginate');

var _mongoosePaginate2 = _interopRequireDefault(_mongoosePaginate);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// make mongoose use bluebird
_mongoose2.default.Promise = _bluebird2.default;

/**
 * Mongoose schema
 */
var schema = _mongoose2.default.Schema({
    _id: {
        type: _mongoose2.default.Schema.Types.ObjectId,
        index: true,
        required: true
    },
    tags: {
        type: [String],
        required: true
    }
});

schema.plugin(_mongoosePaginate2.default);

// create model from schema
var TagModel = _mongoose2.default.model('TagModel', schema);
exports.default = TagModel;